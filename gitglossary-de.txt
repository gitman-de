gitglossary(7)
==============

NAME
----
gitglossary - Ein GIT-Glossar

SYNOPSIS
--------
*

DESCRIPTION
-----------

include::glossary-content.txt[]

SEE ALSO
--------
linkgit:gittutorial[7], linkgit:gittutorial-2[7],
linkgit:everyday[7], linkgit:gitcvs-migration[7],
link:user-manual.html[The Git User's Manual]

GIT
---
Teil der linkgit:git[1]-Suite.
