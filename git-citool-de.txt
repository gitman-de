git-citool(1)
=============

NAME
----
git-gitool - Graphische Alternative zu git-commit

SYNOPSIS
--------
'git citool'

DESCRIPTION
-----------
Eine graphische, Tcl/Tk-basierte Oberfläche, um veränderte Dateien
anzushen, sie in den Index aufzunehmen, eine Änderungsnachricht
einzugeben und neue Änderungen in den aktuellen Zweig einzupflegen.
Diese Oberfläche ist eine alternative zum weniger interaktiven
Programm 'git-commit'.

'git-citool' ist eigentlich ein Standardalias für `git gui citool`.
Siehe linkgit:git-gui[1] für weitere Informationen.

Author
------
Geschrieben von Shawn O. Pearce <spearce@spearce.org>.

Documentation
--------------
Dokumentation von Shawn O. Pearce <spearce@spearce.org>.

GIT
---
Teil der linkgit:git[1]-Suite.
